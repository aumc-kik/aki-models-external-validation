# External Validation and Transportability of Models to Predict Acute Kidney Injury in the Intensive Care Unit

This is an external validation of models for the prediction of AKI in intensive care. The models were developed on [MIMIC](https://mimic.mit.edu/) data and are validated on [the eICU dataset](https://eicu-crd.mit.edu/).

## Running

1. The external validation of the logistic regression (LR) and random forest (RF) can be run via the `eICU_External_Validation_LR_RF.ipynb`. It requires the models `models/LR_model.sav` and `models/random_forest.joblib`
2.	The continuous and before onset LSTM models can be externally validated from `eICU_External_Validation_LSTM.ipynb`. It requires the model in `models/LSTM_model_tuned.pth`
3.	The XGBoost model can be externally validated from `eICU_External_Validation_LXGBoost.ipynb`. It requires the model in `models/xgb_model.json`
4.	The membership model obtained from `Membership_Model.ipynb`.
5.	The class imbalance in the MIMIC, MIMIC training set and eICU can be found in `Class_Imbalance.ipynb`.
6.	The joint calibration plots and predicted probabilities histograms can be generated from `Calibration_Plots.ipynb`.

## Dependencies

- torch
- numpy
- sklearn
- pandas
- matplotlib
- seaborn
- xgboost
- pickle
- joblib

## Data

The notebooks expects the following data generated from eICU:

- `kdigo_stages_measured.csv` Time-series measurements of creatinine, urine output for the last six, 12 and 24 hours and the respective labels.
- `labs-anion-gap.csv` Time-series data of the laboratory tests.
- `vitals.csv` Time-series data of the measurements of vital signs.
- `vents-vasopressor-sedatives-kdigo_stages_measured.csv` Temporal information on whether mechanical ventilation, vasopressor or sedative medications were applied.
- `icustay_detail-kdigo_stages_measured.csv` Non-temporal variables of patient demographics such as: age (numerical), gender (binary), ethnicity group (categorical) and type of admission (categorical).

## Models

Continuous AKI prediction

- LSTM

Prediction 48 hours before the onset of AKI

- LSTM
- XGBoost
- Logistic regression
- Random forest

The code of the original models is openly available at [this repository](https://github.com/mapo89/continuous-aki-predict)

## Cite

If you use our code in your own work please cite our paper [External Validation and Transportability of Models to Predict Acute Kidney Injury in the Intensive Care Unit](https://ebooks.iospress.nl/doi/10.3233/SHTI220683):

    @inproceedings{Vagliano:2022,
         author = {Vagliano, Iacopo and  Byrne Salsas, Carmen and W{\"u}nn, Tina and Schut, Martijn C},
         title = {External Validation and Transportability of Models to Predict Acute Kidney Injury in the Intensive Care Unit},
         booktitle = {Informatics and Technology in Clinical Care and Public Health},
         series = {Studies in health technology and informatics},
         pages = {148--151},
         DOI = {10.3233/SHTI220683},
         volume = {295},
         year = {2022},
         URL = {https://doi.org/10.3233/SHTI220683}
    }